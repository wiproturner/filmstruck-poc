# Film Struck App Automation testing POC

## Introduction

This Repo includes the selenium scripts to perform tests to verify the End to End functional  work flows of Film Struck application 
in web browsers as well as the devices such as tabs and cellphones.

## Table of Contents

* [Prerequisites](#prerequisites)
* [DriversAndBrowsers](#driversandbrowsers)
* [Execution](#execution)
* [LocalSetup](#localsetup)
* [ProjectStructureAndWorkflow](#projectstructureandworkflow)

## Prerequisites

* Maven (if using eclipse, install Maven Integration for Eclipse)
* Java
* WebDriver (Chrome,Firefox,Appium)
* Eclipse / IntelliJ Cucumber- Plugin
* Chrome browser needs to be installed in the Jenkins server as well as local machine.
* Need GeckoDriver in the repo folders.

## DriversAndBrowsers

* Browser used- Chrome, Firefox
* ChromeDriver version - Always downloads the latest one
* GeckoDriver version - Always downloads the latest one

## Execution

* Execution is part of the jenkinsfile where the jenkins file contains commands to execute the maven goals.
* Then the 'mvn clean verify' starts the execution in the chrome browser of jenkins.
* Jenkins will generate a pretty report after the run

## LocalSetup

``` git clone git@gitlab.com:wiproturner/filmstruck-poc.git ```
    Clone the repo using the above command.

## ProjectStructureAndWorkflow

* The test features are located in the features folder with different scenarios
* The test suites are located in the src/test/java/runner.
* The POM consists of 'maven-failsafe-plugin'  for running the the Acceptance suites through the maven in command prompt.(Where also you can specify required the browser/ and versions of the dependencies)


Running it locally through command prompt
_________________________________________

* Open the command prompt in the project location.
* 'mvn clean' -- helps to clean the target folder with the previous results.
* 'mvn verify'  -- This starts the execution process of the tests specified in POM.

Results
_______

* After the execution is completed the results are stored cucumber reports folder in jenkins job.