@FilmSectionTag
@poc
Feature: Movie Details or Film Section
    Film Section of movie contains the Cast Name, Movie Time, Director, Year and type of movie.

  Scenario: Check the Movie Details in Film Section
    Given the user is on the Film Struck Application Homepage
    When the user is in Movie Details Page of Searched Movie
    Then the user checks the Movie Details of Film
    And the user logs out from the Application