@SearchTags
@poc
Feature: Search Field in Film Struck Application
  Search Box is provided with a feature to search for a movie by inserting first 3 letters of a film which in turn displays lists of
  contents of the films.

  Scenario: User searches movie by inserting first 3 letters of a film in the Search Box
    Given the user is on the Film Struck Application Homepage
    When the user enters "hop" three letter of the movie
    Then the user validates "hopscotch" as search result
    And the user logs out from the Application

  Scenario: User searches movie and selects from the search result
    Given the user is on the Film Struck Application Homepage
    When the user enters "hop" three letter of the movie
    Then the user selects "Hopscotch" movie from the search result
    And the user logs out from the Application