@FeatureTags
@poc
Feature: A Featured Tab
  Featured tab contains Carousel Control, Featured Films, Genres, Popuplar , Recently Added information of Movies and
  show about Feature in Film Struck Web Application

  Scenario: Check user can control Carousel to view film details
    Given the user is on the Film Struck Application Homepage
    When the user selects "FEATURED" tab
    Then the user checks the previous and next Carousel Control
    And the user logs out from the Application