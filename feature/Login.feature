@LoginTags
@poc
Feature: Film Struck Login Page
  Check the user is able to login and logout from the Film Struck Application successfully.

  Scenario: Check User can login with valid User Credentials and logout
    Given the user launches the Film Struck Application
    When the user enters email address and password
    Then the user should view Application Homepage
    And the user logs out from the Application

  Scenario Outline: Check user can view invalid user and password error when login with invalid credentials
    Given the user launches the Film Struck Application
    When the user enters "<emailAddress>" and "<password>"
    Then the user can view error message in login page
    And the user closes Film Struck Application
    Examples:
      | emailAddress                  | password   |
      | madhukarchakhaiyar@wipro.com  | wipro      |
      | madhukarchakhaiya@wipro.com   | testing123 |
      | madhukarchak1haiyar@wipro.com | wipro1     |