@ThemesGenres
@poc
Feature: Flims Page -> Filter action
  Films Page under the Film Struck Application contains Filter action. Filter action is used to filter the movie based
  on the type of the movie like Action, Adventure etc.

  Scenario Outline: Filter the type of the movie based on the given Filter
    Given the user is on the Film Struck Application Homepage
    And the user selects "FILMS" tab
    And the user can view "Films"
    When the user select "<filter>" from the dropdown
    Then the user can view selected type of movie in the Films Page
    And the user logs out from the Application
    Examples:
      | filter |
      | War    |
      | action |