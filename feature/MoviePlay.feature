@PlayMovie

Feature: This feature checks user is able to play ,resume and close movie

  Scenario: User is able to play, resume and close the movie
    Given the user is on the Film Struck Application Homepage
    And the user selects "FILMS" tab
    And the user can view "Films"
    When the user clicks on the Play button of the movie
    Then the user can watch, resume and close the movie
    And the user logs out from the Application

