@FilmTags
@poc
Feature: Films Page
  Films Page Contains all the movies available in the Film Struck Application and it allows the user to perform sorting
  on the basis of ascending and descending order and Date. User can filter the movie type, Add to the watchlist
  and can watch the movie.

  Scenario: User is able to add and delete movie from the watchlist
    Given the user is on the Film Struck Application Homepage
    And the user selects "FILMS" tab
    And the user can view "Films"
    When the user adds the movie to the watchlist
    Then the user can view added movie in the watchlist
    And the user can delete movie from the watchlist
    And the user logs out from the Application

  Scenario: User is able to Sort the movie based on A to Z
    Given the user is on the Film Struck Application Homepage
    And the user selects "FILMS" tab
    And the user can view "Films"
    When the user sort movie based on "A-Z"
    Then the user can view "A Mighty Wind" as the First Movie
    And the user logs out from the Application

  Scenario: User is able to Sort the movie based on Z to A
    Given the user is on the Film Struck Application Homepage
    And the user selects "FILMS" tab
    And the user can view "Films"
    When the user sort movie based on "Z-A"
    Then the user can view "Women of Twilight" as the Last Movie
    And the user logs out from the Application

  Scenario: User is able to Sort the movie based on Newest First
    Given the user is on the Film Struck Application Homepage
    And the user selects "FILMS" tab
    And the user can view "Films"
    When the user sort movie based on "Newest First"
    Then the user can view the movie from "2013"
    And the user logs out from the Application