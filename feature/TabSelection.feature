@TabSelection
@poc
Feature: Film Struck Tabs Selection
  In Film struck application we have Featured, Themes, Genres and Films tabs, when the user selects the tabs the
  user can see the respective details.

  Scenario Outline: Check Tab Selections
    Given the user is on the Film Struck Application Homepage
    When the user selects "<tab>" tab
    Then the user can view "<selectedTab>"
    And the user logs out from the Application
    Examples:
      | tab      | selectedTab    |
      | FEATURED | Featured Films |
      | THEMES   | Themes         |
      | Genres   | action         |
      | FILMS    | Films          |