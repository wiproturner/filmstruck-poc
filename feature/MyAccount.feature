@MyAccount
@poc
Feature: My Account Page
  User can edit their personal information, account password and billing information also manage or change their
  subscription details in my Account page.

  Scenario: User can be able to change the account name
    Given the user is on the Film Struck Application Homepage
    And the user navigates to my account page
    When the user edits their account name
    Then the user can see the success message
    And the user confirms the edited name is updated in the Homepage
    And the user logs Out from My Account Page

  Scenario: User can be able to change the account password
    Given the user is on the Film Struck Application Homepage
    And the user navigates to my account page
    When the user changes the account password
    Then the user cancel to change the account password
    And the user logs Out from My Account Page

  Scenario: User can get information about mode of payment
    Given the user is on the Film Struck Application Homepage
    And the user navigates to my account page
    And the user clicks on Billing Information link
    When the user edits the Billing Information
    Then the user can see Credit Card Information and PayPal
    And the user logs Out from My Account Page

  Scenario:User can manage and change the subscription
    Given the user is on the Film Struck Application Homepage
    And the user navigates to my account page
    And the user clicks on manage or change your subscription link
    When the user verifies the subscription details
    Then the user can change the subscription
    And the user logs Out from My Account Page