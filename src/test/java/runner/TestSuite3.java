package runner;

import com.github.mkolisnyk.cucumber.runner.ExtendedCucumberOptions;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@ExtendedCucumberOptions(
        jsonUsageReport = "target/cucumber3.json",
        usageReport = true,
        detailedReport = true,
        detailedAggregatedReport = true,
        overviewReport = false,
        toPDF = false,
        outputFolder = "target"
)
@CucumberOptions(strict = true,plugin = {"html:target/cucumber-html-report3",
        "junit:target/cucumber-junit3.xml",
        "json:target/cucumber3.json",
        "pretty:target/cucumber-pretty3.txt"},
        features = {"feature"},
        glue = {"stepdefiner"},
        tags = {"@poc3"}
)
public class TestSuite3 {
}
