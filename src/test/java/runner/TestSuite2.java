package runner;

import com.github.mkolisnyk.cucumber.runner.ExtendedCucumberOptions;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@ExtendedCucumberOptions(
        jsonUsageReport = "target/cucumber2.json",
        usageReport = true,
        detailedReport = true,
        detailedAggregatedReport = true,
        overviewReport = false,
        toPDF = false,
        outputFolder = "target"
)
@CucumberOptions(strict = true,plugin = {"html:target/cucumber-html-report2",
        "junit:target/cucumber-junit2.xml",
        "json:target/cucumber2.json",
        "pretty:target/cucumber-pretty2.txt"},
        features = {"feature"},
        glue = {"stepdefiner"},
        tags = {"@poc2"}
)
public class TestSuite2 {
}


