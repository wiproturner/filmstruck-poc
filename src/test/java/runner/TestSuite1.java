package runner;

import com.github.mkolisnyk.cucumber.runner.ExtendedCucumberOptions;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@ExtendedCucumberOptions(
        jsonUsageReport = "target/cucumber1.json",
        usageReport = true,
        detailedReport = true,
        detailedAggregatedReport = true,
        overviewReport = false,
        toPDF = false,
        outputFolder = "target"
)
@CucumberOptions(strict=true, plugin = {"html:target/cucumber-html-report1",
        "junit:target/cucumber-junit1.xml",
        "json:target/cucumber1.json",
        "pretty:target/cucumber-pretty1.txt"},
        features = {"feature"},
        glue = {"stepdefiner"},
        tags = {"@poc1"}
)
public class TestSuite1 {
}