package stepdefiner;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import filmstruck.pages.HomePage;
import filmstruck.pages.LoginAndLogoutPage;

public class TabSelectionStep {

    LoginAndLogoutPage loginAndLogoutPage = new LoginAndLogoutPage();
    HomePage homePage = new HomePage();

    @Given("^the user is on the Film Struck Application Homepage$")
    public void theUserIsAtFilmStruckApplicationHomepage() {
        loginAndLogoutPage.launchApplication();
        loginAndLogoutPage.clickLogin();
        loginAndLogoutPage.enterUserDetailsAndSignIn();
        homePage.validateUserName();
    }

    @When("^the user selects \"([^\"]*)\" tab$")
    public void theUserSelectsTab(String selectTab) {
        homePage.selectUserProvidedTab(selectTab);
    }

    @And("^the user can view \"([^\"]*)\"$")
    public void theUserCanViewSelectedTab(String validationText) {
        homePage.validateSelectedTab(validationText);
        homePage.scrollPageToTop();
    }
}
