package stepdefiner;

import cucumber.api.java.en.Then;
import filmstruck.pages.FeaturedPage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FeaturedPageStep {

    FeaturedPage featuredPage = new FeaturedPage();

    Logger logger = LoggerFactory.getLogger(FeaturedPageStep.class);

    @Then("^the user checks the previous and next Carousel Control$")
    public void theUserCheckPreviousAndNextCarouselControl() {
        logger.info("Validating Carousel Movement in Featured Tab");
        featuredPage.checkNextCarouselControl();
        featuredPage.checkPreviousCarouselControl();
    }
}
