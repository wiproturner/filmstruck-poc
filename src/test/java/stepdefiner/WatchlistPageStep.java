package stepdefiner;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import filmstruck.pages.FilmsPage;
import filmstruck.pages.HomePage;
import filmstruck.pages.WatchListPage;

public class WatchlistPageStep {

    WatchListPage watchListPage = new WatchListPage();
    HomePage homePage = new HomePage();
    FilmsPage filmsPage = new FilmsPage();
    int currentCountOfWatchList;

    @When("^the user adds the movie to the watchlist$")
    public void theUserAddsTheMovieToTheWatchlist() {
        filmsPage.addMovieInWatchList();
        currentCountOfWatchList = filmsPage.getCurrentCountOfWatchList();
    }

    @Then("^the user can view added movie in the watchlist$")
    public void theUserCanViewAddedMovieInTheWatchlist() {
        homePage.clickOnTheWatchList();
        watchListPage.validateWatchListPage();
        watchListPage.validateMovieAddedInWatchList();
    }

    @And("^the user can delete movie from the watchlist$")
    public void theUserCanDeleteMovieFromTheWatchlist() {
        watchListPage.userDeleteMovieFromWatchList(currentCountOfWatchList);
        homePage.selectUserProvidedTab("FEATURED");
    }
}
