package stepdefiner;

import cucumber.api.PendingException;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import filmstruck.pages.HomePage;
import filmstruck.pages.MovieDetailsPage;
import filmstruck.utility.Constants;

public class MovieDetailsStep {

    HomePage homePage = new HomePage();
    MovieDetailsPage movieDetailsPage = new MovieDetailsPage();

    @When("^the user is in Movie Details Page of Searched Movie$")
    public void theUserIsInMovieDetailsPageOfMovie() throws Throwable {
        homePage.enterThreeLettersInSearchBox(Constants.MOVIE_FIRST_TREE_LETTER);
        homePage.userViewSearchResult(Constants.MOVIE_NAME);
        homePage.clickOnTheSearch();
        movieDetailsPage.validateMovieDetails();
        movieDetailsPage.validateMovieName(Constants.MOVIE_NAME);
    }

    @Then("^the user checks the Movie Details of Film$")
    public void theUserChecksTheMovieDetailsOfFilm() {
       movieDetailsPage.checkDirectorName(Constants.DIRECTOR_NAME);
       movieDetailsPage.checkTimeOfMovie(Constants.MOVIE_TIME);
       movieDetailsPage.checkCastText(Constants.MOVIE_CAST_TEXT);
       movieDetailsPage.checkMovieTypeAndAvailableMovie();
    }
}
