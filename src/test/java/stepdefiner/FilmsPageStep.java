package stepdefiner;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import filmstruck.pages.FilmsPage;

public class FilmsPageStep {

    FilmsPage filmsPage = new FilmsPage();

    @When("^the user clicks on the Play button of the movie$")
    public void theUserClicksOnThePlayButtonOfTheMovie() {
        filmsPage.clickOnPlayForMovie();
    }

    @And("^the user can watch, resume and close the movie$")
    public void theUserCanWatchResumeAndCloseTheMovie() {
        filmsPage.playMovie();
        filmsPage.resumeMovie();
        filmsPage.closeMovie();
    }

    @When("^the user sort movie based on \"([^\"]*)\"$")
    public void theUserSortMovieBasedOnSortingType(String sortingType) {
        filmsPage.clickOnTheSortingFilterDropdown();
        filmsPage.selectSortingType(sortingType);
    }

    @Then("^the user can view \"([^\"]*)\" as the First Movie$")
    public void theUserCanViewAsTheFirstMovie(String firstMovie) {
        filmsPage.userCheckSorting(firstMovie);
    }

    @Then("^the user can view \"([^\"]*)\" as the Last Movie$")
    public void theUserCanViewAsTheLastMovie(String lastMovie) {
        filmsPage.userCheckSorting(lastMovie);
    }

    @Then("^the user can view the movie from \"([^\"]*)\"$")
    public void theUserCanViewTheMoviesFrom(String date) {
        filmsPage.userCheckDateSorting(date);
    }

    @When("^the user select \"([^\"]*)\" from the dropdown$")
    public void theUserSelectFromTheDropdown(String filter) {
        filmsPage.selectFilterMovieTypeFromDropdown(filter);
    }

    @Then("^the user can view selected type of movie in the Films Page$")
    public void theUserCanViewSelectedTypeOfMovieInTheFilmsPage() {
        filmsPage.validateFilteredMovieType();
    }
}
