package stepdefiner;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import filmstruck.pages.HomePage;
import filmstruck.pages.MovieDetailsPage;

public class HomePageStep {

    HomePage homePage = new HomePage();
    MovieDetailsPage movieDetailsPage = new MovieDetailsPage();

    @When("^the user enters \"([^\"]*)\" three letter of the movie$")
    public void theUserEntersThreeLetterOfTheMovie(String movieThreeLetters) {
        homePage.enterThreeLettersInSearchBox(movieThreeLetters);
    }

    @Then("^the user validates \"([^\"]*)\" as search result$")
    public void theUserValidatesAsSearchResult(String movieName) {
        homePage.userViewSearchResult(movieName);
        homePage.reloadHomePage();
    }

    @Then("^the user selects \"([^\"]*)\" movie from the search result$")
    public void theUserSelectsMovieFromTheSearchResult(String movieName) throws Throwable {
        homePage.userViewSearchResult(movieName);
        homePage.clickOnTheSearch();
        movieDetailsPage.validateMovieDetails();
        movieDetailsPage.validateMovieName(movieName);
    }
}
