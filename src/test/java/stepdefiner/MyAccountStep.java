package stepdefiner;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import filmstruck.pages.HomePage;
import filmstruck.pages.LoginAndLogoutPage;
import filmstruck.pages.MyAccountPage;

public class MyAccountStep {

    HomePage homePage = new HomePage();
    MyAccountPage myaccountpage = new MyAccountPage();
    LoginAndLogoutPage loginLogoutPage = new LoginAndLogoutPage();


    @And("^the user navigates to my account page$")
    public void theUserNavigatesToMyAccountPage() {
        homePage.validateUserName();
        homePage.clickOnTheUserName();
        homePage.clickOnMyAccount();
        myaccountpage.verifyMyAccountHomePage();
    }

    @When("^the user edits their account name$")
    public void theUserEditTheirAccountName() {
        myaccountpage.clickOnEditNameButton();
        myaccountpage.editFirstNameAndConfirm();
    }

    @Then("^the user can see the success message$")
    public void theUserCanSeeTheSuccessMessage() {
        myaccountpage.verifyTheSuccessMessage();
    }

    @And("^the user confirms the edited name is updated in the Homepage$")
    public void theUserConfirmTheEditedNameInTheHomepage() {
        homePage.validateUserName();
    }

    @When("^the user changes the account password$")
    public void theUserChangesTheAccountPassword() {
        myaccountpage.clickOnChangePasswordButton();
    }

    @Then("^the user cancel to change the account password$")
    public void theUserCancelToChangeTheAccountPassword() {
        myaccountpage.clickOnChangePasswordCancelButton();
    }

    @And("^the user clicks on Billing Information link$")
    public void theUserClicksOnBillingInformationLink() {
        myaccountpage.clickOnMyBillingInformationLink();
    }

    @When("^the user edits the Billing Information$")
    public void theUserEditsTheBillingInformation() {
        myaccountpage.clickOnEditBillingInformationButton();
    }

    @Then("^the user can see Credit Card Information and PayPal$")
    public void theUserCanSeeCreditCardInformationAndPayPal() {
        myaccountpage.verifyPaymentModeSection();
    }

    @And("^the user logs Out from My Account Page$")
    public void theUserLogsOutFromMyAccountPage() {
        homePage.validateUserName();
        homePage.clickOnTheUserName();
        loginLogoutPage.myAccountPagelogout();
    }

    @And("^the user clicks on manage or change your subscription link$")
    public void theUserClicksOnManageOrChangeYourSubscriptionLink() {
        myaccountpage.clickOnManageOrChangeYourSubscriptionLink();
    }

    @When("^the user verifies the subscription details$")
    public void theUserCanVerifiesTheSubscriptionDetails() {
        myaccountpage.verifyTheSubscriptionDetails();
    }

    @Then("^the user can change the subscription$")
    public void theUserCanChangeTheSubscription() {
        myaccountpage.changeTheSubscription();
    }
}

