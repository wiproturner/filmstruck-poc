package stepdefiner;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import filmstruck.pages.HomePage;
import filmstruck.pages.LoginAndLogoutPage;

public class LoginStep {

    LoginAndLogoutPage loginAndLogoutPage = new LoginAndLogoutPage();
    HomePage homePage = new HomePage();

    @Given("^the user launches the Film Struck Application$")
    public void theUserLaunchesTheFilmStruckApplication() {
        loginAndLogoutPage.launchApplication();
    }

    @When("^the user enters email address and password$")
    public void theUserEntersEmailAddressAndPassword() {
        loginAndLogoutPage.clickLogin();
        loginAndLogoutPage.enterUserDetailsAndSignIn();
    }

    @Then("^the user should view Application Homepage$")
    public void theUserShouldViewApplicationHomepage() {
        homePage.validateUserName();
    }

    @And("^the user logs out from the Application$")
    public void theUserLogsoutFromTheApplication() {
        homePage.validateUserName();
        homePage.clickOnTheUserName();
        loginAndLogoutPage.logout();
    }

    @When("^the user enters \"([^\"]*)\" and \"([^\"]*)\"$")
    public void theUserEntersEmailAddressAndPassword(String emailAddress, String password) throws Throwable {
        loginAndLogoutPage.clickLogin();
        loginAndLogoutPage.enterUserDetailsAndSignIn(emailAddress, password);
    }

    @Then("^the user can view error message in login page$")
    public void theUserCanViewErrorMessageInLoginPage() {
        loginAndLogoutPage.validateLoginError();
    }

    @And("^the user closes Film Struck Application$")
    public void theUserClosesFilmStruckApplication() {
        loginAndLogoutPage.closeApplication();
    }
}
