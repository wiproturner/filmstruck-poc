package filmstruck.pages;

import filmstruck.utility.Time;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class MovieDetailsPage extends PageObject {

    static int countOfMovieTypeInMovieDetails = 1;
    static int countOfMovieTypeDetailsInPage = 1;
    private final By VALIDATE_MOVIE_COVER = By.xpath(elementLocator.getProperty("movieCover"));
    private final String VALIDATE_MOVIE_NAME = elementLocator.getProperty("movieName");
    private final By COUNT_MOVIE_NAME = By.xpath(elementLocator.getProperty("countMoviesInFilmPage"));
    private final String COUNT_MOVIE_TYPE = elementLocator.getProperty("countMovieTypeInFilmPage");
    private final By MOVIE_DETAILS_HEADER = By.xpath(elementLocator.getProperty("movieDetailsHeader"));
    Logger logger = LoggerFactory.getLogger("MovieDetailsPage");
    String movieTypeInMovieDetails;

    public void validateMovieDetails() {
        try {
            Time.waitUntilElementVisible(VALIDATE_MOVIE_COVER, 10);
        } catch (Exception exception) {
            logger.error("Film details are not loaded"+exception);
            Assert.fail(exception.toString());
        }
    }

    public void validateMovieName(String movieName) {
        try {
            Time.waitUntilElementVisible(getMovieName(movieName), 20);
        } catch (Exception exception) {
            logger.error("Movie name not found"+exception);
            Assert.fail(exception.toString());
        }
    }

    public By getMovieName(String movieName) {
        return By.xpath(VALIDATE_MOVIE_NAME + "'" + movieName + "')]");
    }

    public void checkDirectorName(String directorName) {
        driver.findElement(getDirectorElement(directorName)).isDisplayed();
    }

    private By getDirectorElement(String directorName) {
        return By.xpath("//a[contains(text(),'" + directorName + "')]");
    }

    public void checkTimeOfMovie(String movieTime) {
        driver.findElement(getMovieTimeElement(movieTime)).isDisplayed();
    }

    private By getMovieTimeElement(String movieTime) {
        return By.xpath("//span[contains(text(),'" + movieTime + "')]");
    }

    public void checkCastText(String movieCastText) {
        driver.findElement(getCastText(movieCastText)).isDisplayed();
    }

    private By getCastText(String movieCastText) {
        return By.xpath("//span[contains(text(),'" + movieCastText + "')]");
    }

    public void checkMovieTypeAndAvailableMovie() {
        checkCountOfMovieNameInMovieType();
    }

    private void checkCountOfMovieNameInMovieType() {
        try {
            movieTypeInMovieDetails = driver.findElement(By.xpath("(//section//a[@class='capitalize ng-binding'])[" + countOfMovieTypeInMovieDetails + "]")).getText();
            driver.findElement(By.xpath("(//section//a[@class='capitalize ng-binding'])[" + countOfMovieTypeInMovieDetails + "]")).isDisplayed();
            driver.findElement(By.xpath("(//section//a[@class='capitalize ng-binding'])[" + countOfMovieTypeInMovieDetails + "]")).click();
            Time.waitUntilElementVisible(By.xpath("//section//h1[@class='mr-auto ng-binding']"), 10);
            Time.waitForPageLoad(3000);
            Assert.assertEquals(getCountMovieName(), getCountOfMovieType());
            driver.navigate().back();
            Time.waitUntilElementVisible(VALIDATE_MOVIE_COVER, 10);
            countOfMovieTypeInMovieDetails++;
            checkMovieTypeAndAvailableMovie();
        } catch (Exception exception) {
            logger.info("Validation Successful on Movie Details Page");
        }
    }

    private int getCountOfMovieType() {
        try {
            driver.findElement(By.xpath(COUNT_MOVIE_TYPE + "'" + movieTypeInMovieDetails.toLowerCase() + "')])[" + countOfMovieTypeDetailsInPage + "]"));
            countOfMovieTypeDetailsInPage++;
            getCountOfMovieType();
        } catch (Exception exception) {
            logger.info("Count Successful");
        }
        return (countOfMovieTypeDetailsInPage - 1);
    }

    private int getCountMovieName() {
        try {
            Time.waitUntilElementVisible(MOVIE_DETAILS_HEADER, 20);
        } catch (Exception exception) {
            logger.error("Movie details page not found");
            Assert.fail(exception.toString());
        }
        List<WebElement> filterDropdownList = driver.findElements(COUNT_MOVIE_NAME);
        return filterDropdownList.size();
    }
}
