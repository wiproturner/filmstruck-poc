package filmstruck.pages;

import filmstruck.utility.Time;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class FilmsPage extends PageObject {

    private final By PLAY_MOVIE_BUTTON = By.xpath(elementLocator.getProperty("playMovieButton"));
    private final By RESUME_MOVIE_BUTTON = By.xpath(elementLocator.getProperty("clickResumeButton"));
    private final By CLOSE_MOVIE_BUTTON = By.xpath(elementLocator.getProperty("clickCloseButton"));
    private final By CROSS_MOVIE_ICON = By.xpath(elementLocator.getProperty("crossIcon"));
    private final By ADD_MOVIE_IN_WATCHLIST = By.xpath(elementLocator.getProperty("addFilmPlusWatchList"));
    private final By REMOVE_MOVIE_FROM_WATCHLIST = By.xpath(elementLocator.getProperty("addFilmsMinusWatchList"));
    private final By COUNT_IN_WATCHLIST = By.xpath(elementLocator.getProperty("countInWatchList"));
    private final By FILMS_PAGE_MOVIE_CONTAINER = By.xpath(elementLocator.getProperty("filmsPageMovieContainer"));
    private final By MOVIE_FILTER_DROPDOWN_BUTTON = By.xpath(elementLocator.getProperty("filterDropdownClick"));
    private final By FILTER_DROPDOWN_LIST = By.xpath(elementLocator.getProperty("filterDropdownList"));
    private final By COUNT_MOVIE_NAME = By.xpath(elementLocator.getProperty("countMoviesInFilmPage"));
    private final By SORTING_FILTER_DROPDOWN_BUTTON = By.xpath(elementLocator.getProperty("filmsPageDropdownMovie"));
    private final By FILM_PAGE_SORTING_FILTER = By.xpath(elementLocator.getProperty("filmsPageFilterSortingType"));
    private final String COUNT_MOVIE_TYPE = elementLocator.getProperty("countMovieTypeInFilmPage");
    private final String CHECK_FIRST_MOVIE = elementLocator.getProperty("firstMovieCheck");
    private final String CHECK_MOVIE_BASED_ON_DATE = elementLocator.getProperty("firstMovieBasedOnDate");

    Logger logger = LoggerFactory.getLogger("FilmsPage");
    String countInWatchList;
    String filterDropdownData;
    int convertStringIntoInteger;
    int countMovieType = 1;

    public void clickOnPlayForMovie() {
        driver.findElement(PLAY_MOVIE_BUTTON).click();
    }

    public void playMovie() {
        try {
            Time.waitUntilElementVisible(CROSS_MOVIE_ICON, 10);
            Time.playOrResumeMovieForFifteenSeconds();
        } catch (Exception exception) {
            Assert.fail("Concurrent User limit exceeded!! \n" + exception.toString());
            Assert.fail(exception.toString());
        }
    }

    public void resumeMovie() {
        try {
            Time.waitForPageLoad(2000);
            driver.findElement(CROSS_MOVIE_ICON).click();
            Time.waitUntilElementVisible(RESUME_MOVIE_BUTTON, 10);
            driver.findElement(RESUME_MOVIE_BUTTON).click();
            Time.playOrResumeMovieForFifteenSeconds();
        } catch (Exception exception) {
            logger.error("User is not able to resume movie");
            Assert.fail(exception.toString());
        }
    }

    public void closeMovie() {
        try {
            driver.findElement(CROSS_MOVIE_ICON).click();
            Time.waitUntilElementVisible(CLOSE_MOVIE_BUTTON, 10);
            driver.findElement(CLOSE_MOVIE_BUTTON).click();
            Time.waitForPageLoad(3000);
        } catch (Exception exception) {
            logger.info("User is not close the movie");
            Assert.fail(exception.toString());
        }
    }

    public void addMovieInWatchList() {
        countInWatchList = getCountInWatchList();
        driver.findElement(ADD_MOVIE_IN_WATCHLIST).click();
        Time.waitForPageLoad(3000);
        driver.findElement(REMOVE_MOVIE_FROM_WATCHLIST).isDisplayed();
        checkCountOfWatchListIsIncreasedByOne(countInWatchList);
    }

    private String getCountInWatchList() {
        try {
            driver.findElement(COUNT_IN_WATCHLIST).isDisplayed();
        } catch (Exception exception) {
            return "(0)";
        }
        return driver.findElement(COUNT_IN_WATCHLIST).getText();
    }

    private void checkCountOfWatchListIsIncreasedByOne(String countInWatchList) {
        convertStringIntoInteger = Integer.parseInt(String.valueOf(countInWatchList.charAt(1))) + 1;
        Time.waitForPageLoad(3000);
        driver.findElement(By.xpath("//span[contains(text(),'(" + convertStringIntoInteger + ")')]")).isDisplayed();
    }

    public Integer getCurrentCountOfWatchList() {
        return convertStringIntoInteger;
    }

    public void userCheckSorting(String validationMovie) {
        Time.waitForPageLoad(3000);
        driver.findElement(getSortingData(validationMovie)).isDisplayed();
    }

    private By getSortingData(String validationMovie) {
        return By.xpath(CHECK_FIRST_MOVIE + "'" + validationMovie + "')])[1]");
    }

    public void userCheckDateSorting(String date) {
        Time.waitForPageLoad(3000);
        driver.findElement(getSortingDate(date)).isDisplayed();
    }

    private By getSortingDate(String date) {
        return By.xpath(CHECK_MOVIE_BASED_ON_DATE + "'" + date + "'])[1]");
    }

    public void selectFilterMovieTypeFromDropdown(String filter) {
        filterDropdownData = filter;
        driver.findElement(MOVIE_FILTER_DROPDOWN_BUTTON).isDisplayed();
        driver.findElement(MOVIE_FILTER_DROPDOWN_BUTTON).click();
        getSelectedMovieFromTheFilter(filter);
    }

    private void getSelectedMovieFromTheFilter(String filterDropdownData) {
        List<WebElement> filterDropdownList = driver.findElements(FILTER_DROPDOWN_LIST);
        for (WebElement filter : filterDropdownList) {
            if (filter.getText().equalsIgnoreCase(filterDropdownData)) {
                filter.click();
                break;
            }
        }
    }

    public void validateFilteredMovieType() {
        try {
            Time.waitUntilElementVisible(FILMS_PAGE_MOVIE_CONTAINER, 10);
            Assert.assertEquals(getCountOfMovieNameInFilmPage(), getCountOfMovieTypeInFilmPage());
            logger.info("The expected movie type is dispalyed");
        } catch (Exception exception) {
            logger.info("Invalid movie type");
            Assert.fail(exception.toString());
        }
    }

    public int getCountOfMovieNameInFilmPage() {
        Time.waitForPageLoad(3000);
        List<WebElement> filterDropdownList = driver.findElements(COUNT_MOVIE_NAME);
        return filterDropdownList.size();
    }

    public int getCountOfMovieTypeInFilmPage() {
        try {
            driver.findElement(By.xpath(COUNT_MOVIE_TYPE + "'" + filterDropdownData.toLowerCase() + "')])[" + (countMovieType) + "]"));
            countMovieType++;
            getCountOfMovieTypeInFilmPage();
        } catch (Exception exception) {
            logger.info("Count Successful");
        }
        return (countMovieType - 1);
    }

    public void selectSortingType(String sortingType) {
        List<WebElement> filterDropdownList = driver.findElements(FILM_PAGE_SORTING_FILTER);
        for (WebElement filter : filterDropdownList) {
            if (filter.getText().equalsIgnoreCase(sortingType)) {
                filter.click();
                break;
            }
        }
    }

    public void clickOnTheSortingFilterDropdown() {
        driver.findElement(SORTING_FILTER_DROPDOWN_BUTTON).click();
        Time.waitForPageLoad(3000);
    }
}
