package filmstruck.pages;

import filmstruck.utility.Time;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HomePage extends PageObject {

    private final String LOGIN_USER_NAME = elementLocator.getProperty("userName");
    private final String USER_SELECT_TAB = elementLocator.getProperty("selectedTabLink");

    private final By SEARCH_BOX = By.xpath(elementLocator.getProperty("searchBox"));
    private final By SEARCH_BOX_RESULT = By.xpath(elementLocator.getProperty("searchFieldResult"));
    private final By CLICK_ON_WATCHLIST = By.xpath(elementLocator.getProperty("clickWatchList"));
    private final By MY_ACCOUNT = By.xpath(elementLocator.getProperty("myAccount"));

    Logger logger = LoggerFactory.getLogger("HomePage");

    public void validateUserName() {
        try {
            Time.waitForPageLoad(5000);
            WebElement findUserName = driver.findElement(By.xpath("//span[contains(text(),'madhukar')]"));
            JavascriptExecutor executor = (JavascriptExecutor) driver;
            executor.executeScript("arguments[0].scrollIntoView();", findUserName);
            Time.waitForPageLoad(3000);
            Time.waitUntilElementVisible(By.xpath(LOGIN_USER_NAME), 20);
        } catch (Exception exception) {
            logger.error("Expected username not visble in the homepage" + exception);
            Assert.fail(exception.toString());
        }
    }

    public void clickOnTheUserName() {
        Actions builder = new Actions(driver);
        WebElement hoverOnUserName = driver.findElement(By.xpath(LOGIN_USER_NAME));
        builder.moveToElement(hoverOnUserName).build().perform();
    }

    public void selectUserProvidedTab(String selectTab) {
        String selectTabLinkText = getSelectedTabLinkText(selectTab);
        Time.waitForPageLoad(3000);
        driver.findElement(By.xpath(selectTabLinkText)).click();
    }

    private String getSelectedTabLinkText(String selectTab) {
        return USER_SELECT_TAB + "'" + selectTab + "')]";
    }

    public void validateSelectedTab(String validationText) {
        Time.waitForPageLoad(3000);
        WebElement validateText = driver.findElement(getValidationTextForSelectedTab(validationText));
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].scrollIntoView();", validateText);
        driver.findElement(getValidationTextForSelectedTab(validationText)).isDisplayed();
    }

    public void scrollPageToTop() {
        WebElement scrollToTop = driver.findElement(By.xpath(LOGIN_USER_NAME));
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].scrollIntoView();", scrollToTop);
    }

    private By getValidationTextForSelectedTab(String validationText) {
        return By.xpath("//h1[contains(text(),'" + validationText + "')]");
    }

    public void enterThreeLettersInSearchBox(String movieThreeLetters) {
        driver.findElement(SEARCH_BOX).sendKeys(movieThreeLetters);
        Time.waitForPageLoad(3000);
    }

    public void userViewSearchResult(String movieName) {
        try {
            Time.waitUntilElementVisible(SEARCH_BOX_RESULT, 10);
        } catch (Exception exception) {
            logger.error("Searched result not found" + exception);
            Assert.fail(exception.toString());
        }
        validateMovieName(movieName);
    }

    private void validateMovieName(String movieName) {
        System.out.println(driver.findElement(SEARCH_BOX_RESULT).getText());
        Assert.assertEquals(driver.findElement(SEARCH_BOX_RESULT).getText().toLowerCase(), movieName.toLowerCase());
    }

    public void clickOnTheSearch() {
        try {
            Time.waitUntilElementVisible(SEARCH_BOX_RESULT, 10);
            driver.findElement(SEARCH_BOX_RESULT).click();
            Time.waitForPageLoad(3000);
        } catch (Exception exception) {
            logger.error("Searched movie not found" + exception);
            Assert.fail(exception.toString());
        }
    }

    public void clickOnTheWatchList() {
        driver.findElement(CLICK_ON_WATCHLIST).click();
    }

    public void reloadHomePage() {
        driver.navigate().refresh();
    }

    public void clickOnMyAccount() {
        try {
            Time.waitUntilElementVisible(MY_ACCOUNT, 10);
            driver.findElement(MY_ACCOUNT).click();
        } catch (Exception exception) {
            logger.error("'My Account' is not accessible for the user" + exception);
            Assert.fail(exception.toString());
        }
    }
}
