package filmstruck.pages;

import filmstruck.utility.ConfigReader;
import filmstruck.utility.DriverFactory;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PageObject {

    public static WebDriver driver = null;
    static ConfigReader configReader = new ConfigReader("configAndCredentials.properties");
    public ConfigReader elementLocator = new ConfigReader("elementLocator.properties");
    public Logger logger = LoggerFactory.getLogger("PageObject");
    ConfigReader loginReader = new ConfigReader("configAndCredentials.properties");

    public WebDriver getDriver() {
        try {
            driver = DriverFactory.getLocalWebDriver(System.getProperty("browser"));
            logger.info("Driver Initiated to launch the Browser");
        } catch (Exception exception) {
            logger.error("Driver Initialization failed"+exception);
            Assert.fail(exception.toString());
        }
        return driver;
    }

    public void setDriver() {
        driver = null;
        logger.info("Closing the browser window");
    }
}
