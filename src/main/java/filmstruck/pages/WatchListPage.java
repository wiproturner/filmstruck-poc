package filmstruck.pages;

import filmstruck.utility.Time;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WatchListPage extends PageObject {

    Logger logger = LoggerFactory.getLogger(WatchListPage.class);

    private final By REMOVE_MOVIE_FROM_WATCHLIST = By.xpath(elementLocator.getProperty("addFilmsMinusWatchList"));
    private final By WATCHLIST_HEADER = By.xpath(elementLocator.getProperty("watchListHeader"));
    private final By CLICK_ON_WATCHLIST = By.xpath(elementLocator.getProperty("clickWatchList"));

    public void validateMovieAddedInWatchList() {
        Time.waitForPageLoad(3000);
        driver.findElement(REMOVE_MOVIE_FROM_WATCHLIST).isDisplayed();
    }

    public void validateWatchListPage() {
        try {
            Time.waitUntilElementVisible(WATCHLIST_HEADER,10);
        } catch (Exception exception) {
            logger.error("Watchlist page is not loaded properly");
            Assert.fail(exception.toString());
        }
    }

    public void userDeleteMovieFromWatchList(int currentCountOfWatchList) {
        driver.findElement(REMOVE_MOVIE_FROM_WATCHLIST).click();
        Time.waitForPageLoad(3000);
        checkCountOfWatchListIsDecreasedByOne(currentCountOfWatchList);
    }

    private void checkCountOfWatchListIsDecreasedByOne(int currentCountOfWatchList) {
        currentCountOfWatchList=currentCountOfWatchList-1;
        validateCurrentCount(currentCountOfWatchList);
    }

    private void validateCurrentCount(int currentCountOfWatchList) {
        if(currentCountOfWatchList>=1) {
            driver.findElement(By.xpath("//span[contains(text(),'(" + currentCountOfWatchList + ")')]")).isDisplayed();
        }
        if(currentCountOfWatchList==0){
             driver.findElement(CLICK_ON_WATCHLIST).isDisplayed();
         }
    }
}
