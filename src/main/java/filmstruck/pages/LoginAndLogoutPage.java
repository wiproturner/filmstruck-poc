package filmstruck.pages;

import filmstruck.utility.Time;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoginAndLogoutPage extends PageObject {

    private final By ADMIN_PASSWORD = By.xpath(elementLocator.getProperty("adminAccess"));
    private final By ADMIN_ENTER = By.xpath(elementLocator.getProperty("adminEnterButton"));
    private final By LOGIN_LINK = By.xpath(elementLocator.getProperty("loginLink"));
    private final By EXISTING_USER_TEXT = By.xpath(elementLocator.getProperty("existingUserText"));
    private final By EMAIL_ADDRESS_FIELD = By.id(elementLocator.getProperty("userNameField"));
    private final By PASSWORD_FIELD = By.id(elementLocator.getProperty("userPasswordField"));
    private final By SIGN_IN_BUTTON = By.xpath(elementLocator.getProperty("userSignIn"));
    private final By LOGOUT = By.xpath(elementLocator.getProperty("logout"));
    private final By LOGIN_ERROR_MESSAGE = By.xpath(elementLocator.getProperty("incorrectUserNameAndPasswordError"));
    private final String USER_EMAIL = loginReader.getProperty("userEmailAddress");
    private final String USER_PASSWORD = loginReader.getProperty("userPassword");
    private final String ADMIN_ACCESS_KEY = loginReader.getProperty("adminAccessKey");
    Logger logger = LoggerFactory.getLogger("LoginAndLogoutPage");

    public void clickEnter() {
        try {
            Time.waitUntilElementVisible(ADMIN_ENTER, 10);
            Time.waitForPageLoad(2000);
            driver.findElement(ADMIN_ENTER).click();
        } catch (Exception exception) {
            logger.error("Enter button is not clickable" + exception);
            Assert.fail(exception.toString());
        }
    }

    public void launchApplication() {
        getDriver();
        driver.get(loginReader.getProperty("loginUrl"));
        if (!configReader.getProperty("browser").equalsIgnoreCase("Appium")) {
            driver.manage().window().maximize();
        }
        checkAdminAccesPage();
    }

    private void checkAdminAccesPage() {
        try {
            enterAccessPassword();
            logger.info("Admin Access page is displayed");
        } catch (Exception e) {
            logger.info("Admin Access Page is not displayed");
        }
    }

    public void enterAccessPassword() throws Exception {
        Time.waitUntilElementVisible(ADMIN_PASSWORD, 20);
        Time.waitForPageLoad(3000);
        driver.findElement(ADMIN_PASSWORD).sendKeys(ADMIN_ACCESS_KEY);
        clickEnter();
    }

    public void clickLogin() {
        try {
            Time.waitUntilElementVisible(LOGIN_LINK, 20);
            Time.waitForPageLoad(3000);
            driver.findElement(LOGIN_LINK).click();
            Time.waitUntilElementVisible(EXISTING_USER_TEXT, 20);
            logger.info("Login Page is displayed to the user");
        } catch (Exception exception) {
            logger.info("Login Page not loaded properly");
            Assert.fail(exception.toString());
        }
    }

    public void enterUserDetailsAndSignIn() {
        try {
            Time.waitUntilElementVisible(EXISTING_USER_TEXT, 20);
            driver.findElement(EMAIL_ADDRESS_FIELD).sendKeys(USER_EMAIL);
            driver.findElement(PASSWORD_FIELD).sendKeys(USER_PASSWORD);
            driver.findElement(SIGN_IN_BUTTON).click();
        } catch (Exception exception) {
            logger.error("User Sign in Failed");
            Assert.fail(exception.toString());
        }
    }

    public void logout() {
        try {
            Time.waitUntilElementVisible(LOGOUT, 10);
            driver.findElement(LOGOUT).click();
            Time.waitUntilElementVisible(EXISTING_USER_TEXT, 10);
            driver.quit();
            setDriver();
        } catch (Exception exception) {
            logger.error("User Logout Failed");
            Assert.fail(exception.toString());
        }
    }

    public void enterUserDetailsAndSignIn(String emailAddress, String password) {
        try {
            Time.waitUntilElementVisible(EXISTING_USER_TEXT, 20);
            driver.findElement(EMAIL_ADDRESS_FIELD).sendKeys(emailAddress);
            driver.findElement(PASSWORD_FIELD).sendKeys(password);
            driver.findElement(SIGN_IN_BUTTON).click();
            logger.info("User Login Successful \n");
        } catch (Exception exception) {
            logger.info("User Login Failed \n");
            Assert.fail(exception.toString());
        }
    }

    public void validateLoginError() {
        try {
            Time.waitUntilElementVisible(LOGIN_ERROR_MESSAGE, 10);
            driver.findElement(LOGIN_ERROR_MESSAGE).isDisplayed();
        } catch (Exception exception) {
            Assert.fail("Invalid username and Pasword error message is not displayed \n" + exception.toString());
        }
    }

    public void closeApplication() {
        driver.quit();
        setDriver();
    }

    public void myAccountPagelogout() {
        try {
            driver.findElement(LOGOUT).click();
            Time.waitUntilElementVisible(LOGIN_LINK, 10);
            driver.quit();
            setDriver();
        } catch (Exception exception) {
            logger.error("User Logout Failed");
            Assert.fail(exception.toString());
        }
    }
}
