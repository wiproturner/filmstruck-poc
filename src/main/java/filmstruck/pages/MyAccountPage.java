package filmstruck.pages;

import filmstruck.utility.Constants;
import filmstruck.utility.Time;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.Assert.assertTrue;

public class MyAccountPage extends PageObject {

    private final By MY_ACCOUNT = By.xpath(elementLocator.getProperty("myAccountDetails"));
    private final By EDIT_NAME = By.xpath(elementLocator.getProperty("editNameButton"));
    private final By CHANGE_ACCOUNT_NAME = By.xpath(elementLocator.getProperty("changeaccountname"));
    private final By EDIT_FIRST_NAME = By.xpath(elementLocator.getProperty("editfirstname"));
    private final By EDIT_NAME_CONFIRM_BUTTON = By.xpath(elementLocator.getProperty("editNameConfirmButton"));
    private final By SUCCESS_MESSAGE = By.xpath(elementLocator.getProperty("successMessage"));
    private final By OK_BUTTON = By.xpath(elementLocator.getProperty("okButton"));
    private final By CHANGE_PASSWORD = By.xpath(elementLocator.getProperty("changePasswordButton"));
    private final By CHANGE_PASSWORD_CANCEL_BUTTON = By.xpath(elementLocator.getProperty("userChangePasswordCancelButton"));
    private final By CHANGE_PASSWORD_SECTION = By.xpath(elementLocator.getProperty("changePasswordSection"));
    private final By BILLING_INFORMATION_LINK = By.xpath(elementLocator.getProperty("billingInformationLink"));
    private final By PAYMENT_METHOD_SECTION = By.xpath(elementLocator.getProperty("paymentMethodSection"));
    private final By EDIT_BILLING_INFORMATION_BUTTON = By.xpath(elementLocator.getProperty("editBillingInformation"));
    private final By EDIT_BILLING_INFORMATION_SECTION = By.xpath(elementLocator.getProperty("editBillingInformationSection"));
    private final By CREDIT_CARD_SECTION = By.xpath(elementLocator.getProperty("creditCardSection"));
    private final By PAYPAL_IMAGE = By.xpath(elementLocator.getProperty("payPalImage"));
    private final By GO_TO_PAYPAL = By.xpath(elementLocator.getProperty("goToPayPalButton"));
    private final By MANAGE_OR_CHANGE_SUBSCRIPTION = By.xpath(elementLocator.getProperty("manageOrChangeSubscription"));
    private final By SUBSCRIPTION_DETAILS = By.xpath(elementLocator.getProperty("subscriptionDetails"));
    private final By PACKAGE_DETAILS = By.xpath(elementLocator.getProperty("packageDetails"));
    private final By SELECTED_PACKAGE = By.xpath(elementLocator.getProperty("selectedPackage"));
    private final By CHANGE_SUBSCRIPTION = By.xpath(elementLocator.getProperty("changeSubscription"));
    private final By CONFIRM_TO_CHANGE_SUBSCRIPTION = By.xpath(elementLocator.getProperty("confirmToChangeSubscription"));
    private final By CHANGE_SUBSCRIPTION_CONFIRM_BUTTON = By.xpath(elementLocator.getProperty("changeSubscriptionConfirmButton"));
    private final By GO_TO_BILLING_INFO_BUTTON = By.xpath(elementLocator.getProperty("goToBillingInformationPage"));
    Logger logger = LoggerFactory.getLogger("MyAccountPage");

    public void verifyMyAccountHomePage() {
        try {
            Time.waitUntilElementVisible(MY_ACCOUNT, 10);
        } catch (Exception exception) {
            logger.error("My Account Page is not visible");
            Assert.fail(exception.toString());
        }
    }

    public void clickOnEditNameButton() {
        try {
            Time.waitUntilElementVisible(EDIT_NAME, 10);
        } catch (Exception exception) {
            logger.error("User not able to click on Edit Name button");
            Assert.fail(exception.toString());
        }
        driver.findElement(EDIT_NAME).click();
    }

    public void editFirstNameAndConfirm() {
        try {
            Time.waitUntilElementVisible(CHANGE_ACCOUNT_NAME, 15);
            Time.waitForPageLoad(3000);
            driver.findElement(EDIT_FIRST_NAME).clear();
            driver.findElement(EDIT_FIRST_NAME).sendKeys(Constants.FIRST_NAME);
            driver.findElement(EDIT_NAME_CONFIRM_BUTTON).click();
        } catch (Exception exception) {
            logger.error("User is not able to change the name");
            Assert.fail(exception.toString());
        }
    }

    public void verifyTheSuccessMessage() {
        try {
            Time.waitUntilElementVisible(SUCCESS_MESSAGE, 10);
            driver.findElement(OK_BUTTON).click();
        } catch (Exception exception) {
            logger.error("Success message is not visible");
            Assert.fail(exception.toString());
        }
    }

    public void clickOnChangePasswordButton() {
        try {
            Time.waitUntilElementVisible(CHANGE_PASSWORD, 10);
            driver.findElement(CHANGE_PASSWORD).click();
        } catch (Exception exception) {
            logger.error("Not able to click on Change Password");
            Assert.fail(exception.toString());
        }
    }

    public void clickOnChangePasswordCancelButton() {
        try {
            Time.waitUntilElementVisible(CHANGE_PASSWORD_SECTION, 10);
            driver.findElement(CHANGE_PASSWORD_CANCEL_BUTTON).click();
        } catch (Exception exception) {
            logger.error("User is not able to click on the Change Password Cancel Button");
            Assert.fail(exception.toString());
        }
    }

    public void clickOnMyBillingInformationLink() {
        try {
            driver.findElement(BILLING_INFORMATION_LINK).click();
            Time.waitUntilElementVisible(PAYMENT_METHOD_SECTION, 20);
        } catch (Exception exception) {
            logger.error("Not able to click on the My Billing Information Link in My Account Page");
            Assert.fail(exception.toString());
        }
    }

    public void clickOnEditBillingInformationButton() {
        try {
            Time.waitUntilElementVisible(EDIT_BILLING_INFORMATION_BUTTON, 20);
            driver.findElement(EDIT_BILLING_INFORMATION_BUTTON).click();
            Time.waitUntilElementVisible(EDIT_BILLING_INFORMATION_SECTION, 20);
        } catch (Exception exception) {
            logger.error("Not able to Edit Billing Information in My Account Page");
            Assert.fail(exception.toString());
        }
    }

    public void verifyPaymentModeSection() {
        try {
            Time.waitUntilElementVisible(CREDIT_CARD_SECTION, 10);
            driver.findElement(PAYPAL_IMAGE).click();
            Time.waitUntilElementVisible(GO_TO_PAYPAL, 20);
        } catch (Exception exception) {
            logger.error("Payment Mode is not available in My Account Page");
            Assert.fail(exception.toString());
        }
    }

    public void clickOnManageOrChangeYourSubscriptionLink() {
        try {
            driver.findElement(MANAGE_OR_CHANGE_SUBSCRIPTION).click();
            Time.waitUntilElementVisible(SUBSCRIPTION_DETAILS, 20);
        } catch (Exception exception) {
            logger.error("Manage Or Change your subscription link not found");
            Assert.fail(exception.toString());
        }
    }

    public void verifyTheSubscriptionDetails() {
        try {
            Time.waitUntilElementVisible(PACKAGE_DETAILS, 20);
            String packagedetails = driver.findElement(PACKAGE_DETAILS).getText();
            assertTrue(packagedetails.contains(Constants.PACKAGE_INFO));
            driver.findElement(SELECTED_PACKAGE).isDisplayed();
            logger.info("Subscription validation successful for logged in User");
        } catch (Exception exception) {
            Assert.fail("Subscription expired for the user, please get a new subscription \n"+exception.toString());
        }
    }

    public void changeTheSubscription() {
        driver.findElement(CHANGE_SUBSCRIPTION).click();
        try {
            Time.waitUntilElementVisible(CONFIRM_TO_CHANGE_SUBSCRIPTION, 20);
            driver.findElement(CHANGE_SUBSCRIPTION_CONFIRM_BUTTON).click();
            Time.waitUntilElementVisible(GO_TO_BILLING_INFO_BUTTON, 20);
            driver.findElement(GO_TO_BILLING_INFO_BUTTON).click();
            Time.waitUntilElementVisible(PAYMENT_METHOD_SECTION, 10);
        } catch (Exception exception) {
            logger.error("User is not able to change the subscription");
            Assert.fail(exception.toString());
        }
    }
}