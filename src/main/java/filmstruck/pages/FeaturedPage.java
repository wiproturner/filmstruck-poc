package filmstruck.pages;

import filmstruck.utility.Time;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FeaturedPage extends PageObject {

    Logger logger = LoggerFactory.getLogger(FeaturedPage.class);

    private By PREVIOUS_CAROUSEL_CONTROL = By.xpath(elementLocator.getProperty("previousSlider"));
    private By NEXT_CAROUSEL_CONTROL = By.xpath(elementLocator.getProperty("nextSlider"));
    private By PRE_VALIDATE_CAROUSEL_CONTROL = By.xpath(elementLocator.getProperty("validateSliderDotPrevious"));
    private By NEXT_VALIDATE_CAROUSEL_CONTROL = By.xpath(elementLocator.getProperty("validateSliderDotNext"));
    private String SLIDER_BAR = (elementLocator.getProperty("sliderBar"));

    public void checkNextCarouselControl() {
        searchSliderDot();
        driver.findElement(NEXT_CAROUSEL_CONTROL).click();
        driver.findElement(NEXT_VALIDATE_CAROUSEL_CONTROL).isDisplayed();
    }

    public void checkPreviousCarouselControl() {
        driver.findElement(NEXT_CAROUSEL_CONTROL).click();
        driver.findElement(NEXT_CAROUSEL_CONTROL).click();
        driver.findElement(PREVIOUS_CAROUSEL_CONTROL).click();
        driver.findElement(PRE_VALIDATE_CAROUSEL_CONTROL).isDisplayed();
    }

    public void searchSliderDot() {
        Time.waitForPageLoad(3000);
        WebElement element = driver.findElement(By.xpath(SLIDER_BAR));
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].scrollIntoView();", element);
    }
}
