package filmstruck.utility;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigReader {

    private final Properties properties;

    public ConfigReader(final String propertyFile) {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propertyFile);
        properties = new Properties();
        try {
            properties.load(inputStream);
        } catch (final IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String getProperty(final String propertiesName) {
        return properties.getProperty(propertiesName);
    }
}
