package filmstruck.utility;

public class Constants {

    public static final String FIRST_NAME = "madhukar";
    public static final String PACKAGE_INFO = "Filmstruck Monthly Subscription";

    public static final String MOVIE_FIRST_TREE_LETTER = "hop";
    public static final String MOVIE_NAME = "Hopscotch";
    public static final String DIRECTOR_NAME = "Ronald Neame";
    public static final String MOVIE_TIME= "1h 41m";
    public static final String MOVIE_CAST_TEXT="Cast";

}
