package filmstruck.utility;

import filmstruck.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Time extends PageObject {

    public static void waitUntilElementVisible(By webElement, long timeOutInSeconds) throws Exception {
        WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
        wait.until(ExpectedConditions.visibilityOfElementLocated(webElement));
    }

    public static void waitForPageLoad(long timoutInMiliSeconds) {
        try {
            Thread.sleep(timoutInMiliSeconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void playOrResumeMovieForFifteenSeconds() {
        try {
            Thread.sleep(15000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
