package filmstruck.utility;

import io.appium.java_client.android.AndroidDriver;
import io.github.bonigarcia.wdm.*;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

public class DriverFactory {

    public static WebDriver driver = null;

    public DriverFactory() {
    }

    public static WebDriver getLocalWebDriver(String driverChoice) {
        switch (driverChoice) {
            case "Chrome":
                ChromeDriverManager.getInstance().setup();
                ChromeOptions options=new ChromeOptions();
                options.addArguments("disable-infobars");
                driver = new ChromeDriver(options);
                break;
            case "Firefox":
                System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + File.separator + "driver" + File.separator + "geckodriver.exe");
                driver = new FirefoxDriver();
                break;
            case "IE":
                InternetExplorerDriverManager.getInstance().setup(Architecture.x32);
                driver = new InternetExplorerDriver();
                break;
            case "Edge":
                EdgeDriverManager.getInstance().setup();
                driver = new EdgeDriver();
                break;
            case "Opera":
                OperaDriverManager.getInstance().setup();
                driver = new OperaDriver();
                break;
            case "Appium":
                DesiredCapabilities capabilities = new DesiredCapabilities();
                capabilities.setCapability("platformName", "Android");
                capabilities.setCapability("deviceName", "Android Emulator");
                capabilities.setCapability("platformVersion", "8.1.0");
                capabilities.setCapability("browserName", "Chrome");
                driver = new AndroidDriver(capabilities);
                break;
            default:
                throw new IllegalArgumentException(
                        "Invalid driver. Choose between " + "Chrome, " + "Firefox, " + "IE, " + "Edge " + "or Opera");
        }
        return driver;
    }

    private static WebDriver getRemoteWebDriver(String driverChoice, String seleniumHubUrl) {
        DesiredCapabilities cap = DesiredCapabilities.firefox();
        switch (driverChoice) {
            case "Chrome":
                cap = DesiredCapabilities.chrome();
                break;
            case "Firefox":
                break;
            case "IE":
                cap = DesiredCapabilities.internetExplorer();
                cap.setVersion("ANY");
                cap.setPlatform(Platform.ANY);
                break;
            default:
                throw new IllegalArgumentException(
                        "Invalid driver. Choose between " + "Chrome, " + "Firefox, " + "or IE");
        }
        try {
            driver = new RemoteWebDriver(new URL(seleniumHubUrl), cap);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
        return driver;
    }
}


